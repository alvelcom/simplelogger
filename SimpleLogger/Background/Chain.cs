﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleLogger.Background
{

    public class Chain
    {
        public Formatter Formatter { get; set; }
        public Backend Backend { get; set; }

        public Chain(Formatter f, Backend b)
        {
            Formatter = f;
            Backend = b;
        }
    }
}
        
