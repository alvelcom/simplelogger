﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleLogger.Background.Formatters
{
    class Descriptive : Formatter
    {
        public string Format(Message message)
        {
            var sb = new StringBuilder();
            sb.Append(message.time.ToString());
            sb.AppendFormat(" [{0}] {1} ", message.severity.ToString(), message.realm);

            if (message.logFile != null || message.logLine.HasValue || message.logMethod != null)            
                sb.AppendFormat("File {0} at {1} in {2}", 
                    message.logFile ?? "??", 
                    (message.logLine.HasValue ? message.logLine.ToString() : "??"),
                    message.logMethod ?? "??");
            
            sb.AppendLine();
            sb.AppendLine(message.message);

            if (message.exception != null)
            {
                sb.AppendFormat("Exception \"{0}\" from project {1}, method {2}\n", 
                    message.exception.Message, 
                    message.exception.Source,
                    message.exception.TargetSite);
                sb.AppendLine(message.exception.StackTrace);
            }

            return sb.ToString();            
        }
    }
}
