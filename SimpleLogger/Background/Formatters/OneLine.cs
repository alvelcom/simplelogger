﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleLogger.Background.Formatters
{
    public class OneLine : Formatter
    {
        public string TimeFormat;

        public OneLine(string timeFormat = "g")
        {
            TimeFormat = timeFormat;
        }

        public string Format(Message message)
        {
            return message.time.ToString(TimeFormat)
                + " [" + message.severity.ToString() + "] "
                + message.message
                + (message.exception == null
                    ? ""
                    : " (" + message.exception.Message + ")");
        }
    }
}
