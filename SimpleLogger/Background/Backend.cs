﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleLogger.Background
{
    public interface Backend
    {
        Severity LoggingLevel { get; set; }
        void Write(Severity severity, string message);
    }
}
