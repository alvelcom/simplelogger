﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleLogger.Background
{
    public class Message
    {
        public DateTime time = DateTime.Now;
        public Severity severity = Severity.Debug;
        public Exception exception;
        public string logFile;
        public string logMethod;
        public int? logLine;
        public string realm = "";
        public string message;
    }

    public class Worker
    {
        public List<Chain> Chains { get; set; }
        public Severity SystemLevel;

        public Worker(Severity severity = Severity.Info)
        {
            SystemLevel = severity;
            Chains = new List<Chain>();
        }

        public void Publish(Message message)
        {
            
            if (message.severity < SystemLevel)
                return;

            foreach (var chain in Chains)
            {
                var text = chain.Formatter.Format(message);
                chain.Backend.Write(message.severity, text);
            }
        }
    }
}
