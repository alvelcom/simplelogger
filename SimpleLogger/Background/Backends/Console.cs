﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleLogger.Background.Backends
{
    public class Console : Backend
    {
        public Severity LoggingLevel { get; set; }

        public Console(Severity s = Severity.Info)
        {
            LoggingLevel = s;
        }

        public void Write(Severity severity, string message)
        {
            if (severity < LoggingLevel)
                return;

            System.Console.WriteLine(message);
        }
    }
}
