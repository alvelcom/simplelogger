﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace SimpleLogger.Background.Backends
{
    public class File : Backend
    {
        public Severity LoggingLevel { get; set; }
        
        private FileStream file;
        private string filename;
        public string FileName
        {
            get
            {
                return filename;
            }
            set
            {
                filename = value;
                file = new FileStream(filename, FileMode.Append, FileAccess.Write, FileShare.Write);
            }
        }

        public File(string filename, Severity s = Severity.Info)
        {
            LoggingLevel = s;
            FileName = filename;
        }

        public void Write(Severity severity, string message)
        {
            if (severity < LoggingLevel)
                return;

            var bytes = System.Text.Encoding.UTF8.GetBytes(message);
            lock (this)
            {
                file.Write(bytes, 0, bytes.Length);
                bytes = System.Text.Encoding.UTF8.GetBytes("\n");
                file.Write(bytes, 0, bytes.Length);
            }
        }
    }
}
