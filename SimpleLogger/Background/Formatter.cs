﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleLogger.Background
{
    public interface Formatter
    {
        string Format(Message message);
    }
}
