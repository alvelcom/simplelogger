﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleLogger.Background;
using SimpleLogger.Background.Formatters;

namespace SimpleLogger
{
    public static class LoggerEasyStart
    {
        public static void ConsoleAndFile(string fileBaseName, 
            Severity severity = Severity.Info)
        {
            var wrk = Logger.Worker;
            wrk.Chains.Clear();

            wrk.SystemLevel = severity;
            
            var consoleLine = new Chain(
                new OneLine("T"), 
                new Background.Backends.Console());
            consoleLine.Backend.LoggingLevel = severity;

            wrk.Chains.Add(consoleLine);

            var fileInfoLog = new Chain(
                new Descriptive(), 
                new Background.Backends.File(fileBaseName));
            fileInfoLog.Backend.LoggingLevel = severity;
            wrk.Chains.Add(fileInfoLog);            
        }

        public static void ConsoleOnly(Severity severity = Severity.Info)
        {
            var wrk = Logger.Worker;
            wrk.Chains.Clear();

            wrk.SystemLevel = severity;

            var consoleLine = new Chain(
                new OneLine("T"),
                new Background.Backends.Console());
            consoleLine.Backend.LoggingLevel = severity;
            wrk.Chains.Add(consoleLine);
        }
    }
}
