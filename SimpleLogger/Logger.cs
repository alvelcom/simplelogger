﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using SimpleLogger.Background;

namespace SimpleLogger
{
    public enum Severity
    {
        Debug = 10,
        Info  = 20,
        Alert = 30,
        Error = 40,
        Critical = 50
    }



    public static class Logger
    {
        public static Worker Worker = new Worker();

        public static void Log(Severity severity, 
            string msg, 
            Exception exception = null,
            string logFile = null,
            string logMethod = null,
            int? logLine = null)
        {
            var message = new Message();
            message.severity = severity;
            message.message = msg;
            message.exception = exception;
            message.logFile = logFile;
            message.logMethod = logMethod;
            message.logLine = logLine;

#if DEBUG
            if (logFile == null)
            {
                var stack = new StackTrace();
                var member = stack.GetFrame(2);
                message.logFile = member.GetFileName();
                message.logMethod = member.GetMethod().Name;
                if (0 >= (message.logLine = member.GetFileLineNumber()))
                    message.logLine = null;
            }
#endif
            Worker.Publish(message);
        }


        // Debug
        public static void Debug(string msg, params object[] args)
        {
#if DEBUG
            Log(Severity.Debug, String.Format(msg, args));
#endif
        }

        public static void Debug(Exception ex, string msg, params object[] args)
        {
#if DEBUG
            Log(Severity.Debug, String.Format(msg, args), exception: ex);
#endif
        }


        // Info
        public static void Info(string msg, params object[] args)
        {
            Log(Severity.Info, String.Format(msg, args));
        }

        public static void Info(Exception ex, string msg, params object[] args)
        {
            Log(Severity.Info, String.Format(msg, args), exception: ex);
        }


        // Alert
        public static void Alert(string msg, params object[] args)
        {
            Log(Severity.Alert, String.Format(msg, args));
        }

        public static void Alert(Exception ex, string msg, params object[] args)
        {
            Log(Severity.Alert, String.Format(msg, args), exception: ex);
        }


        // Error
        public static void Error(string msg, params object[] args)
        {
            Log(Severity.Error, String.Format(msg, args));
        }

        public static void Error(Exception ex, string msg, params object[] args)
        {
            Log(Severity.Error, String.Format(msg, args), exception: ex);
        }


        // Critical
        public static void Critical(string msg, params object[] args)
        {
            Log(Severity.Critical, String.Format(msg, args));
        }

        public static void Critical(Exception ex, string msg, params object[] args)
        {
            Log(Severity.Critical, String.Format(msg, args), exception: ex);
        }
    }
}
