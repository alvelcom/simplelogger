﻿# SimpleLogger

A simple C# logging suite inspired by [lager](https://github.com/basho/lager).

# Example

Add to your startup code
```
using SimpleLogger;
...

LoggerEasyStart.ConsoleAndFile("info.log");
```

Then you could call
```
Logger.Info("Some info");
Logger.Error("Error value = {0}", 4);

catch (Exception ex)
{
    Logger.Error(ex, "Some fault occurs in proccesing of {0}", someItemName);
}
```

and so on :)

See LoggerExampleProject for details of use.

