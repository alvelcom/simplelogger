﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SimpleLogger; // <-- this one

namespace LoggerExampleProject
{
    class Program
    {
        static void Main(string[] args)
        {
            // and this one
            LoggerEasyStart.ConsoleAndFile("debug.log", Severity.Debug); 
    
            // Now we can use logs!

    
            Logger.Info("Program is just starts");
            for (int i = 0; i < 3; i++)
            {
                System.Threading.Thread.Sleep(1000);
                Logger.Debug("i = {0}", i);
            }

            try
            {
                SomeFun(10);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Ou! Some error :-(");
            }           

        }

        static void SomeFun(int c)
        {
            throw new Exception("bad value c = " + c.ToString());
        }
    }
}
